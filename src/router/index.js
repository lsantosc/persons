import Vue from 'vue'
import Router from 'vue-router'
// Pages
import Index from '@/pages/Index'
import New from '@/pages/New'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index
    }, 
    { 
      path: '/new',
      name: 'New',
      component: New
    }
  ]
})
