import Vue from 'vue'
import VueI18n from 'vue-i18n'
import VeeValidate from 'vee-validate'
import App from './App'
import router from './router'

Vue.use(VueI18n)
Vue.use(VeeValidate, {
  aria: true,
  classNames: {},
  delay: 0,
  dictionary: null,
  events: 'input|blur',
  locale: 'pt-br',
  i18nRootkey: 'validations',
  strict: true,
  validity: false
})

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
